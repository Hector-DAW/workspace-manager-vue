import Vue from 'vue'
import Router from 'vue-router'
import Register from '../components/Register'
import Login from '../components/Login'
import Calendar from '../components/calendar'
import PersonalData from '../components/PersonalData'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: Calendar
    },
    {
      path: '/personal-data',
      name: 'PersonalData',
      component: PersonalData
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
